﻿using CommandLine;

namespace Builder
{ 
    

class BaseVerb
{
    [Option('p', "path", Required = true, HelpText = "Full path to the Mod Loader Repo")]
    public string ModLoaderRepo { get; set; }
}
class Code : BaseVerb
{
    [Option("nuget", Required = true, HelpText = "Full path to nuget.exe")]
    public string NuGetPath { get; set; }

    [Option("dlls", Required = false, HelpText = "Full path to where the dependencies are stored")]
    public string DllsPath { get; set; }
}

[Verb("builddll", HelpText = "Builds ModLoader.dll file")]
class BuildDll : Code
{
    [Option("msbuild", Required = true, HelpText = "Full path to MSBuild.exe")]
    public string MsBuildPath { get; set; }
}

[Verb("buildwpf", HelpText = "Builds the WPF application")]
class BuildWpf : Code
{
    [Option('o', "output", Required = true, HelpText = "Full path to where to place the exe when finished")]
    public string OutputPath { get; set; }

    [Option("dotnet", Required = true, HelpText = "Full path to dotnet.exe")]
    public string DotNetPath { get; set; }
}

[Verb("buildpatcher", HelpText = "Builders the VTPatcher.dll")]
class BuildPatcher : Code
{
    [Option("msbuild", Required = true, HelpText = "Full path to MSBuild.exe")]
    public string MsBuildPath { get; set; }
}

[Verb("buildunpatcher", HelpText = "Builds the UnPatcher.exe")]
class BuildUnPatcher : Code
{
    [Option("msbuild", Required = true, HelpText = "Full path to MSBuild.exe")]
    public string MsBuildPath { get; set; }
}

[Verb("package", HelpText = "Packages it all up for a release")]
class Package : BaseVerb
{
    [Option('o', "output", Required = true, HelpText = "Full path to where to place the zip when finished")]
    public string OutputPath { get; set; }

    [Option('t', "template", Required = true, HelpText = "Full path to where the template for un buildable files are")]
    public string TemplatePath { get; set; }
}
}